#pragma once
#include <string>
#include <iostream>


#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <mutex>

#include <thread>
#include "SpatialFrame.h"
// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512

class NDI_SerialStream
{
public:
	std::mutex dataAccess;
	bool dataRecieved = false;

	NDI_SerialStream(std::string _ip, std::string _port)
	{
		ip = _ip;
		port = _port;
		initializeWinsock();
		createSocket();
		validateAddress();
		attemptConnection();
	}

	~NDI_SerialStream()
	{
		stopStreamRead();
		closeSocket();
	}

	void startStreamRead()
	{
		stopStream = false;
		streamThread = std::thread(&NDI_SerialStream::doWork,this);
	}

	void stopStreamRead()
	{
		stopStream = true;
		try
		{
			streamThread.join();
		}
		catch (...)
		{
		}
	}

	void closeSocket()
	{
		try
		{
			closesocket(ConnectSocket);
		}
		catch (...) {}

		try
		{
			WSACleanup();
		}
		catch (...) {}
	}

	SpatialFrame getSensorData(int sensorID)
	{
		dataAccess.lock();
		SpatialFrame localCop = sensorData[sensorID];
		dataAccess.unlock();
		return localCop;
	}

private:
	std::string ip;
	std::string port;
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *validAddress = NULL;
	bool stopStream = false;
	std::thread streamThread;

	SpatialFrame sensorData[4] = { SpatialFrame(),SpatialFrame(),SpatialFrame(),SpatialFrame() };

	void initializeWinsock()
	{
		int iResult;
		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
		}
	}

	void createSocket()
	{
		ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("Error at socket(): %ld\n", WSAGetLastError());
			WSACleanup();
		}
	}

	void validateAddress()
	{
		struct addrinfo *result = NULL,
			hints;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		int iResult = getaddrinfo(ip.c_str(), port.c_str(), &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed: %d\n", iResult);
			WSACleanup();
		}
		validAddress = result;
	}

	bool attemptConnection()
	{
		if(validAddress == NULL){return false;}

		
		int iResult = connect(ConnectSocket, validAddress->ai_addr, (int)validAddress->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
		}
	}

	void doWork()
	{
		int recvbuflen = DEFAULT_BUFLEN;
		char recvbuf[DEFAULT_BUFLEN];

		do {
			int iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
			if (iResult > 0)
			{
				std::string inMessage(recvbuf);
				std::vector<std::string> messages = split(inMessage.c_str(), ';');
				for (int i = 0; i < messages.size(); i++)
				{
					std::vector<std::string> data = split(messages[i].c_str(), ',');
					if (data.size() == 15)
					{
						dataAccess.lock();
						try
						{
							int id = std::stoi(data[0]);
							try
							{
								sensorData[id].id = std::stoi(data[0]);
								sensorData[id].x = std::stod(data[1]);
								sensorData[id].y = std::stod(data[2]);
								sensorData[id].z = std::stod(data[3]);
								int index = 4;
								for (int a = 0; a < 3; a++)
								{
									for (int b = 0; b < 3; b++)
									{
										sensorData[id].rotMatrix[b][a] = std::stod(data[index]);
										index++;
									}
								}

								sensorData[id].quality = std::stoi(data[index]); index++;
								sensorData[id].time = std::stod(data[index]);
								dataRecieved = true;
							}
							catch (...)
							{
								sensorData[id] = SpatialFrame(); //if there is an error just set sensor data to a blank spatialframe
							}
						}
						catch (...) {}

						dataAccess.unlock();
					}
					else
					{
						//std::cout << "Bad Message: " << data.size() << std::endl;
					}
				}
			}
				//printf("Bytes received: %d\n", iResult);
			//else if (iResult == 0)
				//printf("Connection closed\n");
			//else
				//printf("recv failed: %d\n", WSAGetLastError());
		} while (!stopStream);
	}

	std::vector<std::string> split(const char *str, char c = ' ')
	{
		std::vector<std::string> result;

		do
		{
			const char *begin = str;

			while (*str != c && *str)
				str++;

			result.push_back(std::string(begin, str));
		} while (0 != *str++);

		return result;
	}
};