add_library(FileIO fileIO.cpp)
target_link_libraries (FileIO LINK_PUBLIC DataObjects ${VTK_DICOM_LIBRARIES} ${PCL_LIBRARIES})
target_include_directories (FileIO PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})