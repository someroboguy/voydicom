#include "fileIO.h"
#include <windows.h>
#include <fstream>

#include <vtkSmartPointer.h>
#include <vtkDICOMImageReader.h>
#include <vtkImageViewer2.h>
#include <vtkImageReader2.h>
#include "vtkImageData.h"
#include "vtkErrorObserver.h"
#include <vtkFileOutputWindow.h>

#include <vtkDICOMMetaData.h>
#include "vtkDICOMDirectory.h"
#include "vtkDICOMReader.h"
#include "vtkDICOMItem.h"

#include "vtkStringArray.h"
#include "vtkIntArray.h"





std::string fileIO::pointCloudDir = "../PointClouds/";



bool fileIO::loadFromPCD(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, std::string filename)
{
	if (cloud->points.size() > 0) { cloud->points.clear(); }
	std::string fullPath = filename;
	if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos) //checks to see if you provided the full path
	{
		fullPath = pointCloudDir; //if you did not provide a full path it gives the path to the PointClouds folder
		fullPath.append(filename);
	}
	std::cout << "load path: " << fullPath << std::endl;
	pcl::PCDReader pcdInput = pcl::PCDReader();
	try
	{
		pcdInput.read(fullPath, *cloud);
		return true;
	}
	catch (...)
	{
		return false;
	}
}
bool fileIO::saveToPCD(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, bool binaryFlag, std::string filename)
{
	if (cloud->points.size() == 0) { return false; }
	std::string fullPath = filename;
	if (filename.find("/") == std::string::npos && filename.find("\\") == std::string::npos)
	{
		fullPath = pointCloudDir;
		fullPath.append(filename);
	}
	std::cout << "save path: " << fullPath << std::endl;
	pcl::PCDWriter pcdOutput = pcl::PCDWriter();
	if (binaryFlag)
	{
		pcdOutput.writeBinary<pcl::PointXYZI>(fullPath, *cloud);
	}
	else
	{

		cloud->width = cloud->points.size();
		cloud->height = 1;
		pcdOutput.writeASCII<pcl::PointXYZI>(fullPath, *cloud);
	}
	return true;
}

bool fileIO::dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

void fileIO::writeCloudNum() {
	int meshNum = 0;
	std::string tmp;
	ifstream readFile;
	readFile.open("../Calibration/RotationMatricies.txt");
	while (std::getline(readFile, tmp)) {
		if (tmp[0] != '-') {
			std::string empty = "";
			empty += tmp[0];
			meshNum = std::stoi(empty); //when you get to a cloud number, replace the holder with that number
		}
	}
	meshNum++; //increment to next set of data
	readFile.close();

	ofstream write("../Calibration/RotationMatricies.txt", std::ios_base::app | std::ios_base::out); //opens in append mode
	write << meshNum << std::endl;
	write.close();
}


void fileIO::writeTransformationMat(Eigen::Matrix4f transMat, std::string filename, int model) {

	ofstream write(filename, std::ios_base::app | std::ios_base::out); //opens in append mode
	write << model << std::endl;
	write << "[" << transMat(0,0) << " " << transMat(0, 1) << " " << transMat(0, 2) << " " //x column
		<< transMat(1, 0) << " " << transMat(1, 1) << " " << transMat(1, 2) << " " //y column
		<< transMat(2, 0) << " " << transMat(2, 1) << " " << transMat(2, 2) << " ]["
		<< transMat(0, 3) << " " << transMat(1, 3) << " " << transMat(2, 3) << " ]" << std::endl;
	write.close();
}


void fileIO::readTransformationMat(Eigen::Matrix4f& transMat, std::string filename, int model) {

	ifstream read;
	read.open(filename);
	bool cont = true;
	std::string temp;
	while ( cont && std::getline(read, temp)) { //getting to the correct transformation matrix
		int num = -1;
		if (temp.length() == 1) {
			num = std::stoi(temp);
		}
		if (num == model) {
			cont = false;
		}
	}

	std:getline(read, temp);
	int index = 1;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			std::string holder = "";
			while (temp[index] != ' ') {
				holder += temp[index];
				index++;
			}
			transMat(i, j) = std::stod(holder);
			index++;
		}
	}
	index += 2;
	for (int i = 0; i < 3; i++) {
		std::string holder = "";
		while (temp[index] != ' ') {
			holder += temp[index];
			index++;
		}
		index++;
		transMat(i, 3) = std::stod(holder);
	}
}

void fileIO::writeFrame(pcl::PointCloud<pcl::PointXYZ>::Ptr ransacCloud, int index, SpatialFrame frame) { //write the rotation. Need to pass it a point cloud
	/*
	Layout of file storage.
	#(this is the pointcloud #)
	-#-[(this is the index number) [rotation matrix] 
	*/
	ofstream write("../Calibration/RotationMatricies.txt", std::ios_base::app | std::ios_base::out); //opens in append mode
	write << "-" << index << "-[" 
		  << frame.rotMatrix[0][0] << " " << frame.rotMatrix[0][1] << " " << frame.rotMatrix[0][2] << " " //x column
	      << frame.rotMatrix[1][0] << " " << frame.rotMatrix[1][1] << " " << frame.rotMatrix[1][2] << " " //y column
		  << frame.rotMatrix[2][0] << " " << frame.rotMatrix[2][1] << " " << frame.rotMatrix[2][2] << " ]["
		  << frame.x << " " << frame.y << " " << frame.z << " ]" << std::endl;
	write.close();
	//need to write the index and all the rotation and translation information for each sensor point.
}

void fileIO::readFrame(double rotMat[3][3], double trans[3], int cloudNum, std::string index) { // find the rotation index
	//index is passed as a string to compare later.
	std::string tmp;
	ifstream readFile;
	readFile.open("../Calibration/RotationMatricies.txt");
	bool cont = true; //stops the while loop after it finds and writes the index
	int cloudNumHolder = 0;
	while (cloudNumHolder != cloudNum) { //gets to the correct line
		std::getline(readFile, tmp);
		if (tmp[0] != '-') { 
			std::string empty = "";
			empty += tmp[0];
			cloudNumHolder = std::stoi(empty); }
	}

	while (std::getline(readFile, tmp) && cont) {
		if (tmp[0] == '-') {
			bool match = true;
			int i = 1;
			for (i = 1; i <= index.length() && match; i++) { //check to make sure that the current index matches the found index.
				if (tmp[i] != index[i - 1]) {
					match = false;
				}
			}
			if (tmp[i] != '-') {//check if the number had an extra digit
				match = false;
			}
			if (match) { //if you found a match
				i += 2; //to get to the first digit
				for (int j = 0; j < 3; j++) { //parse through each
					for (int k = 0; k < 3; k++) {
						std::string holder = "";
						while (tmp[i] != ' ') {
							holder += tmp[i];
							i++;
						}
						if (tmp[i] == ' '){ //if this was a space skip and go on to next iteration
							i++;
							rotMat[j][k] = std::stod(holder);
						}
					}
				} //after you filled the rotation matrix, fill the translation matrix.
				i += 2;
				for (int j = 0; j < 3; j++) {
					std::string holder = "";
					while ((tmp[i] != ' ') && (tmp[i] != ']')) {
						holder += tmp[i];
						i++;
					}
					if (tmp[i] == ' ') { //if this was a space skip and go on to next iteration
						i++;
						trans[j] = std::stod(holder);
					}
				}

				cont = false;
				readFile.close();
			}
		}
	}
}

bool fileIO::loadFromDICOMDirectory(StructuredVolume &vol, std::string dicomDirectory)
{
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);

	//dicom read
	if (!dirExists(dicomDirectory.c_str()))
	{
		std::cout << "Directory Invalid" << std::endl;
		return false;
	}

	vtkFileOutputWindow *outwin = vtkFileOutputWindow::New();
	outwin->SetFileName("vtklog.txt");
	outwin->SetInstance(outwin);

	vtkSmartPointer<ErrorObserver>  errorObserver = vtkSmartPointer<ErrorObserver>::New();
	vtkSmartPointer<vtkDICOMImageReader> reader = vtkSmartPointer<vtkDICOMImageReader>::New();

	reader->AddObserver(vtkCommand::ErrorEvent, errorObserver);
	reader->AddObserver(vtkCommand::WarningEvent, errorObserver);
	
	vtkSmartPointer<vtkImageData> sliceData = vtkSmartPointer<vtkImageData>::New();
	
	reader->SetDirectoryName(dicomDirectory.c_str());
	reader->Update();


	if (errorObserver->GetError() || errorObserver->GetWarning())
	{
		std::cout << "Failed DICOM file access" << std::endl;
		return false;
	}
	
	sliceData = reader->GetOutput();
	outwin->Delete();

	//extracting pointcloud
	int numberOfDims = sliceData->GetDataDimension();
	int * dims = sliceData->GetDimensions();
	std::cout << "Cloud dimensions: ";
	int totalPoints = 1;
	for (int i = 0; i < numberOfDims; i++)
	{
		std::cout << dims[i] << " , ";
		totalPoints = totalPoints * dims[i];
	}
	std::cout << std::endl;

	std::cout << "Number of dicom points: " << totalPoints << std::endl;
	//generate pcl point cloud
	double* dataRange = sliceData->GetScalarRange();
	double* spacingData = reader->GetDataSpacing();
	std::cout << "Data intensity bounds... min: " << dataRange[0] << ", max: " << dataRange[1] << std::endl;
	if (numberOfDims != 3)
	{
		std::cout << "Incorrect number of dimensions in dicom file, generation failed..." << std::endl;
		return false;
	}
	else
	{
		double xScale = spacingData[0];
		double yScale = spacingData[1];
		double zScale = spacingData[2];

		std::cout << "x spacing: " << xScale << std::endl;
		std::cout << "y spacing: " << yScale << std::endl;
		std::cout << "z spacing: " << zScale << std::endl;

		for (int z = 0; z < dims[2]; z++)
		{
			if (z % 50 == 0)
			{
				double percentageComplete = (double)z / (double)dims[2];
				std::cout << "Dicom Read Progress: " << (int)(100.0 * percentageComplete) << "%" << std::endl;
			}
			for (int y = 0; y < dims[1]; y++)
			{
				for (int x = 0; x < dims[0]; x++)
				{
					pcl::PointXYZI tempPt = pcl::PointXYZI();
					tempPt.x = x * xScale;
					tempPt.y = y * yScale;
					tempPt.z = z * zScale;
					double tempIntensity = sliceData->GetScalarComponentAsDouble(x, y, z, 0);
					if (!isinf(tempIntensity))
					{
						tempPt.intensity = tempIntensity;
					}
					else
					{
						tempPt.intensity = 0;
					}
					
					cloud->points.push_back(tempPt);
				}
			}
		}
	}
	vol = StructuredVolume(cloud, pcl::PointXYZ(spacingData[0], spacingData[1], spacingData[2]), pcl::PointXYZ(dims[0], dims[1], dims[2]), pcl::PointXYZ(), dataRange[0], dataRange[1]);
	std::cout << "Total points loaded: " << cloud->points.size() << std::endl;
	return true;
}

bool fileIO::loadFromDICOMDirectory_V2(StructuredVolume &vol, std::string dicomDirectory)
{
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);

	if (!dirExists(dicomDirectory.c_str()))
	{
		std::cout << "Directory Invalid" << std::endl;
		return false;
	}

	// find all DICOM files supplied by the user
	vtkSmartPointer<vtkDICOMDirectory> ddir =
		vtkSmartPointer<vtkDICOMDirectory>::New();
	ddir->SetDirectoryName(dicomDirectory.c_str());
	ddir->Update();

	int n = ddir->GetNumberOfPatients();
	int patientSelection = 0;
	if (n > 1)
	{
		std::cout << "Select Patient number, total count: " << n << std::endl;
		std::string userInput;
		std::getline(std::cin, userInput);
		patientSelection = std::stoi(userInput);
	}

	const vtkDICOMItem& patientItem = ddir->GetPatientRecord(patientSelection);
	std::cout << "Patient " << patientSelection << ": "<< patientItem.Get(DC::PatientID).AsString() << "\n";

	vtkIntArray *studies = ddir->GetStudiesForPatient(patientSelection);
	vtkIdType m = studies->GetMaxId() + 1;

	int studySelection = 0;
	if (m > 1)
	{
		std::cout << "Select study, total count: " << m << std::endl;
		std::string userInput;
		std::getline(std::cin, userInput);
		studySelection = std::stoi(userInput);
	}

	int j = studies->GetValue(studySelection);
	const vtkDICOMItem& studyItem = ddir->GetStudyRecord(j);
	const vtkDICOMItem& studyPItem = ddir->GetPatientRecordForStudy(j);
			cout << " Study " << j << ": \""
				<< studyItem.Get(DC::StudyDescription).AsString() << "\" \""
				<< studyPItem.Get(DC::PatientName).AsString() << "\" "
				<< studyItem.Get(DC::StudyDate).AsString() << "\n";
	int k0 = ddir->GetFirstSeriesForStudy(j);
	int k1 = ddir->GetLastSeriesForStudy(j);

	int seriesSelection;
	std::cout << "Select series, range: " << k0<<" to "<<k1 << std::endl;
	for (int i = k0; i <= k1; i++)
	{
		const vtkDICOMItem& seriesItem = ddir->GetSeriesRecord(i);
		vtkStringArray *a = ddir->GetFileNamesForSeries(i);
		
		cout << "  Series " << i << ": \""
			<< seriesItem.Get(DC::SeriesDescription).AsString() << "\" "
			<< seriesItem.Get(DC::SeriesNumber).AsString() << " "
			<< seriesItem.Get(DC::Modality).AsString() << ", Images: "
			<< a->GetNumberOfTuples() << "\n";
	}

	std::string userInput;
	std::getline(std::cin, userInput);
	seriesSelection = std::stoi(userInput);

	const vtkDICOMItem& seriesItem = ddir->GetSeriesRecord(seriesSelection);
	cout << "  Series " << seriesSelection << ": \""
		<< seriesItem.Get(DC::SeriesDescription).AsString() << "\" "
		<< seriesItem.Get(DC::SeriesNumber).AsString() << " "
		<< seriesItem.Get(DC::Modality).AsString() << "\n";

	vtkStringArray *a = ddir->GetFileNamesForSeries(seriesSelection);
	vtkDICOMReader *reader = vtkDICOMReader::New();
	reader->SetFileNames(a);
	reader->Update();

	vtkSmartPointer<vtkImageData> sliceData = reader->GetOutput();

	int numberOfDims = sliceData->GetDataDimension();
	int * dims = sliceData->GetDimensions();
	std::cout << "Cloud dimensions: ";
	int totalPoints = 1;
	for (int i = 0; i < numberOfDims; i++)
	{
		std::cout << dims[i] << " , ";
		totalPoints = totalPoints * dims[i];
	}
	std::cout << std::endl;

	std::cout << "Number of dicom points: " << totalPoints << std::endl;
	//generate pcl point cloud
	double* dataRange = sliceData->GetScalarRange();
	double* spacingData = reader->GetDataSpacing();
	std::cout << "Data intensity bounds... min: " << dataRange[0] << ", max: " << dataRange[1] << std::endl;
	if (numberOfDims != 3)
	{
		std::cout << "Incorrect number of dimensions in dicom file, generation failed..." << std::endl;
		return false;
	}
	else
	{
		double xScale = spacingData[0];
		double yScale = spacingData[1];
		double zScale = spacingData[2];

		std::cout << "x spacing: " << xScale << std::endl;
		std::cout << "y spacing: " << yScale << std::endl;
		std::cout << "z spacing: " << zScale << std::endl;

		for (int z = 0; z < dims[2]; z++)
		{
			if (z % 50 == 0)
			{
				double percentageComplete = (double)z / (double)dims[2];
				std::cout << "Dicom Read Progress: " << (int)(100.0 * percentageComplete) << "%" << std::endl;
			}
			for (int y = 0; y < dims[1]; y++)
			{
				for (int x = 0; x < dims[0]; x++)
				{
					pcl::PointXYZI tempPt = pcl::PointXYZI();
					tempPt.x = x * xScale;
					tempPt.y = y * yScale;
					tempPt.z = z * zScale;
					double tempIntensity = sliceData->GetScalarComponentAsDouble(x, y, z, 0);
					if (!isinf(tempIntensity))
					{
						tempPt.intensity = tempIntensity;
					}
					else
					{
						tempPt.intensity = 0;
					}

					cloud->points.push_back(tempPt);
				}
			}
		}
	}
	vol = StructuredVolume(cloud, pcl::PointXYZ(spacingData[0], spacingData[1], spacingData[2]), pcl::PointXYZ(dims[0], dims[1], dims[2]), pcl::PointXYZ(), dataRange[0], dataRange[1]);
	std::cout << "Total points loaded: " << cloud->points.size() << std::endl;
	return true;
}