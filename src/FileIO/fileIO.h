#pragma once
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include "StructuredVolume.h"
#include "SpatialFrame.h"


class fileIO
{
public:
	
	static bool loadFromPCD(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, std::string filename);
	static bool saveToPCD(pcl::PointCloud<pcl::PointXYZI>::Ptr cloud, bool binaryFlag, std::string filename);
	static void writeCloudNum();
	static void writeTransformationMat(Eigen::Matrix4f transMat, std::string filename, int model);
	static void readTransformationMat(Eigen::Matrix4f& transMat, std::string filename, int model);
	static void writeFrame(pcl::PointCloud<pcl::PointXYZ>::Ptr ransacCloud, int index, SpatialFrame frame);
	static void readFrame(double rotMat[3][3], double trans[3], int cloudNum, std::string index);
	//loads from standard dicom directory structure
	static bool loadFromDICOMDirectory(StructuredVolume &vol, std::string dicomDirectory);
	static bool loadFromDICOMDirectory_V2(StructuredVolume &vol, std::string dicomDirectory);
	//standard binary pcd read (single dimensional), but adds points to the end of the file that specifies dicom data structure.
	//static bool loadDICOMCloud(StructuredVolume &vol, std::string filename);
	//standard binary pcd write(single dimensional), but looks for points at the end of the file that specifies dicom data structure.
	//static bool saveDICOMCloud(StructuredVolume vol, std::string filename);

private:
	static bool dirExists(const std::string& dirName_in);
	static std::string pointCloudDir;
};

