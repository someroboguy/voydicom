
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>

#include <vtkSmartPointer.h>
#include <vtkDICOMImageReader.h>
#include "vtkImageData.h"

#include "fileIO.h"
#include "StructuredVolume.h"

#include <windows.h>

// --------------
// -----Main-----
// --------------
int main(int argc, char** argv)
{
	StructuredVolume dicomData = StructuredVolume();
	fileIO::loadFromDICOMDirectory_V2(dicomData, "C:/Local Software/voyagervision/resources/DICOM_Samples/Second Attempt/CD1-Axial Only");
	system("PAUSE");
	return 0;
}
