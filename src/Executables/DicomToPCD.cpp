
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>

#include <vtkSmartPointer.h>
#include <vtkDICOMImageReader.h>
#include "vtkImageData.h"

#include <windows.h>

// --------------
// -----Main-----
// --------------
int main(int argc, char** argv)
{
	bool binaryFlag = false;
	bool directoryFlag = false;
	bool fileNameFlag = false;
	bool singleSliceFlag = false;
	bool thresholdFlag = false;
	int sliceIndex = 0;
	int lowIntensity = 0;
	int highIntensity = 0;
	char dicomDirectory[256];
	std::string outputFileName = "dicomCloud.pcd";
	
	//parse input
	if (pcl::console::find_argument(argc, argv, "-h") >= 0 || argc == 1)
	{
		std::cout << "\n\nUsage: " << "DicomToPCD" << " [options]\n\n"
			<< "Options:\n"
			<< "-------------------------------------------\n"
			<< "-h           this help\n"
			<< "-b           binary output file specification\n"
			<< "-d           dicom directory to be specified\n"
			<< "-f           output file name to be specified\n"
			<< "-t           generates file with one slice only (specify slice number in following argument)\n"
			<< "-r           specifies specific range of intensities to include in cloud (low and high bound in following two arguments)\n"
			<< "\n\n";
		system("PAUSE");
		return 0;
	}


	//file save type (ascii default, binary option)
	if (pcl::console::find_argument(argc, argv, "-b") >= 0)
	{
		binaryFlag = true;
		std::cout << "Binary pcd file type\n";
	}
	else
	{
		std::cout << "ASCII pcd file type\n";
	}

	//allows for a single slice to be grabbed (for testing and smaller files)
	if (pcl::console::find_argument(argc, argv, "-t") >= 0)
	{
		int argIndex = pcl::console::find_argument(argc, argv, "-t");
		sliceIndex = std::atoi(argv[argIndex+1]);
		singleSliceFlag = true;
		std::cout << "Single Slice Output, slice selected: "<<sliceIndex<<std::endl;
	}

	if (pcl::console::find_argument(argc, argv, "-r") >= 0)
	{
		thresholdFlag = true;
		int argIndex = pcl::console::find_argument(argc, argv, "-r");
		lowIntensity = std::atoi(argv[argIndex+1]);
		highIntensity = std::atoi(argv[argIndex+2]);
		std::cout << "Threshold mode, bounds: " << lowIntensity<<" To "<< highIntensity << std::endl;
	}

	//dicom directory name(current called directory default, specified as -d otherwise)
	if (pcl::console::find_argument(argc, argv, "-d") >= 0)
	{
		directoryFlag = true;
		std::cout << "Please specify dicom directory below followed by cariage return..." << std::endl;
		std::cin.getline(dicomDirectory, 256);
		std::cout << "Dicom directory specified as: \n" << dicomDirectory << std::endl;
	}
	else
	{
		std::cout << "Dicom directory not specified, current directory used \n";
	}
	//debug
	
	std::string debugDir = "C:/Local Software/voyagervision/resources/DICOM_Samples/Second Attempt/CD1-Axial Only";
	strcpy(dicomDirectory, debugDir.c_str());
	directoryFlag = true;
	


	//output file name(dicomCloud.pcd default, specified as -f followed by desired name otherwise)
	if (pcl::console::find_argument(argc, argv, "-f") >= 0)
	{
		fileNameFlag = true;
		std::cout << "Please specify output file name below followed by cariage return..." << std::endl;
		char userInput[256];
		std::cin.getline(userInput, 256);
		outputFileName = userInput;
		std::cout << "File output specified as: \n" << outputFileName << std::endl;
	}
	else
	{
		std::cout << "No file name specified, dicomCloud.pcd to be generated \n";
	}
	
	//dicom read

	vtkSmartPointer<vtkDICOMImageReader> reader = vtkSmartPointer<vtkDICOMImageReader>::New();
	if (directoryFlag)
	{
		reader->SetDirectoryName(dicomDirectory);
	}
	else
	{
		std::string currentDir = _pgmptr;
		currentDir = currentDir.substr(0, currentDir.find_last_of("\\")) + "\\";
		reader->SetDirectoryName(currentDir.c_str());
	}
	reader->Update();
	vtkSmartPointer<vtkImageData> sliceData = reader->GetOutput();

	

	int numberOfDims = sliceData->GetDataDimension();
	int * dims = sliceData->GetDimensions();
	std::cout << "Cloud dimensions: ";
	int totalPoints = 1;
	for (int i = 0; i < numberOfDims; i++)
	{
		std::cout << dims[i] << " , ";
		totalPoints = totalPoints * dims[i];
	}
	std::cout << std::endl;

	std::cout << "Number of dicom points: " << totalPoints << std::endl;

	//generate pcl point cloud
	
	pcl::PointCloud<pcl::PointXYZI> dicomIntensityCloud = pcl::PointCloud<pcl::PointXYZI>();
	
	double* dataRange = sliceData->GetScalarRange();
	std::cout << "Data intensity bounds... min: " << dataRange[0] << ", max: " << dataRange[1] << std::endl;
	if (numberOfDims != 3) 
	{ 
		std::cout << "Incorrect number of dimensions in dicom file, generation failed..." << std::endl; 
		return 0;
	}
	else
	{
		double* spacing = reader->GetDataSpacing();
		double xScale = spacing[0];
		double yScale = spacing[1];
		double zScale = spacing[2];
		std::cout << "x spacing: " << xScale << std::endl;
		std::cout << "y spacing: " << yScale << std::endl;
		std::cout << "z spacing: " << zScale << std::endl;

		for (int z = 0; z < dims[2]; z++)
		{
			if (singleSliceFlag)
			{
				z = sliceIndex;
			}
			if (z%50==0)
			{
				double percentageComplete = (double)z / (double)dims[2];
				std::cout << "Dicom Read Progress: " << (int)(100.0 * percentageComplete) << "%" << std::endl;
			}
			for (int y = 0; y < dims[1]; y++)
			{
				for (int x = 0; x < dims[0]; x++)
				{
					pcl::PointXYZI tempPt = pcl::PointXYZI();
					tempPt.x = x * xScale;
					tempPt.y = y * yScale;
					tempPt.z = z * zScale;
					double tempIntensity = sliceData->GetScalarComponentAsDouble(x, y, z, 0);
					if (!isinf(tempIntensity))
					{
						tempPt.intensity = tempIntensity;
					}
					else
					{
						tempPt.intensity = 0;
					}
					if (thresholdFlag)
					{
						if (tempIntensity > lowIntensity && tempIntensity < highIntensity)
						{
							dicomIntensityCloud.points.push_back(tempPt);
						}
					}
					else
					{
						dicomIntensityCloud.points.push_back(tempPt);
					}
				}
			}
			if (singleSliceFlag) { break; }
		}
	}

	std::cout << "output points: " << dicomIntensityCloud.points.size() << std::endl;

	std::cout << "Writing file..." << std::endl;
	//generate pcd file output
	pcl::PCDWriter pcdOutput = pcl::PCDWriter();
	if (!binaryFlag)
	{
		dicomIntensityCloud.width = dicomIntensityCloud.points.size();
		dicomIntensityCloud.height = 1;
		pcdOutput.writeASCII<pcl::PointXYZI>(outputFileName, dicomIntensityCloud);
	}
	else
	{
		pcdOutput.writeBinary<pcl::PointXYZI>(outputFileName, dicomIntensityCloud);
	}

	std::string outputDirectory = _pgmptr;
	outputDirectory = outputDirectory.substr(0, outputDirectory.find_last_of("\\"));

	std::cout << "file written to: \n"<< outputDirectory<<"\\"<<outputFileName<<std::endl;
	system("PAUSE");
	return 0;
}
