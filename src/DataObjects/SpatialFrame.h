#pragma once
#include <string>
class SpatialFrame
{
public:
	int id;
	double x;
	double y;
	double z;
	double rotMatrix[3][3];
	int quality;
	double time;

	SpatialFrame(int _id, double _x, double _y, double _z, double _rot[3][3], int _quality, double _time)
	{
		id = _id;
		x = _x;
		y = _y;
		z = _z;
		for (int i = 0; i < 3; i++)
		{
			for (int ii = 0; ii < 3; ii++)
			{
				rotMatrix[i][ii] = _rot[i][ii];
			}
		}
		quality = _quality;
		time = _time;
	}
	SpatialFrame()
	{
		id = 0;
		x = 0;
		y = 0;
		z = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int ii = 0; ii < 3; ii++)
			{
				if(i!=ii){ rotMatrix[i][ii] = 0; }
				else { rotMatrix[i][ii] = 1; }
				
			}
		}
		quality = 0;
		time = 0;
	}
	std::string toString()
	{

		char output[256];

		sprintf(output, "[%d] %8.3f %8.3f %8.3f ::  [%u] , %8.3f",
			id,x,y,z,quality,time);
		return std::string(output);
	}

};

