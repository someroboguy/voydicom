#pragma once
#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>

class StructuredVolume
{
public:
	pcl::PointCloud<pcl::PointXYZI>::Ptr cloud;
	pcl::PointXYZ dataSpacing;
	pcl::PointXYZ dataDimensions;
	pcl::PointXYZ dataOrigin;

	double maxVal;
	double minVal;

	
	StructuredVolume()
	{
		cloud.reset(new pcl::PointCloud<pcl::PointXYZI>);
		dataSpacing = pcl::PointXYZ();
		dataDimensions = pcl::PointXYZ();
		dataOrigin = pcl::PointXYZ();

		maxVal = 0;
		minVal = 0;
	}
	

	StructuredVolume(pcl::PointCloud<pcl::PointXYZI>::Ptr _cloud, pcl::PointXYZ _dataSpacing, pcl::PointXYZ _dataDimensions, pcl::PointXYZ _dataOrigin, double _minVal, double _maxVal )
	{
		cloud.reset(new pcl::PointCloud<pcl::PointXYZI>);
		pcl::copyPointCloud<pcl::PointXYZI, pcl::PointXYZI>(*_cloud, *cloud);
		pcl::copyPoint<pcl::PointXYZ, pcl::PointXYZ>(_dataSpacing, dataSpacing);
		pcl::copyPoint<pcl::PointXYZ, pcl::PointXYZ>(_dataDimensions, dataDimensions);
		pcl::copyPoint<pcl::PointXYZ, pcl::PointXYZ>(_dataOrigin, dataOrigin);
		maxVal = _maxVal;
		minVal = _minVal;
		std::cout << "debug" << std::endl;
	}

	int indexAt(int x, int y, int z)
	{
		int outIndex = 0;
		outIndex += x;
		outIndex += y * (int)dataDimensions.x;
		outIndex += z * (int)dataDimensions.x * (int)dataDimensions.y;
		return outIndex;
	}
	/*
	int* splitIndicies(int totalIndex)
	{
		int indicies[] = { 0,0,0 };


		indicies[2] = floor(totalIndex / dataDimensions.z);
		totalIndex = totalIndex % (int)dataDimensions.z;
		indicies[1] = floor(totalIndex / dataDimensions.y);
		totalIndex = totalIndex % (int)dataDimensions.y;
		indicies[0] = floor(totalIndex / dataDimensions.x);
	}
	*/
};